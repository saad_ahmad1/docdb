import React from 'react'
import {Table, Row, Container} from "react-bootstrap"
import TransitRow from './transit_row'
import ModelFooter from './../../components/model_footer.js'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { searchClient } from './../../components/SearchClient.js'
import algoliaImg from './../../components/algolia.svg'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Popup from './../../components/popup.js';
import './../Styles/mod_styles.css'

class Transit extends React.Component {
    constructor(props) {
        super(props)
        this.myRef = React.createRef();
        this.state = {
            total: 0,
            routes: [],
            pages: 0,
            page: 1,
            search: '',
            city_filter: null,
            sortBy: 'id',
            descend: false,
            compare: [],
            popup: false,
          }
    }

    componentDidMount() {
        this.getRoutesList()
    }

    getRoutesList() {
        let strlst = []
        if(this.state.search) {
            strlst.push(this.state.search)
        }
        if(this.state.city_filter) {
            strlst.push(this.state.city_filter)
        }
        const str = strlst.join()

        let index = searchClient.initIndex('transit_connections')
        if(this.state.descend) {
            if(this.state.sortBy === 'id') {
                index = searchClient.initIndex('transit_connections_desc')
            }
            if(this.state.sortBy === 'city') {
                index = searchClient.initIndex('transit_connection_city_desc')
            }
            if(this.state.sortBy === 'origin') {
                index = searchClient.initIndex('transit_connection_company_name_desc')
            }
            if(this.state.sortBy === 'destination') {
                index = searchClient.initIndex('transit_connection_doctor_name_desc')
            }
        }
        else {
            if(this.state.sortBy === 'city') {
                index = searchClient.initIndex('transit_connection_city_asc')
            }
            if(this.state.sortBy === 'origin') {
                index = searchClient.initIndex('transit_connection_company_name_asc')
            }
            if(this.state.sortBy === 'destination') {
                index = searchClient.initIndex('transit_connection_doctor_name_asc')
            }
        }

        // algolia zero indexes pages
        const page = this.state.page - 1
        index.search(str, {page: page})
        .then(result => this.setState({routes: result.hits, total: result.nbHits, pages: result.nbPages, page: result.page + 1}))
    }

    // sorting options
    sortBy(event) {
        this.setState({page: 1, sortBy: event.target.value}, () => this.getRoutesList())
    }

    // filtering options for city
    filterByCity(event) {
        this.setState({city_filter: event.target.value}, () => this.getRoutesList())
    }

    handleSearch(event) {
        this.setState({search: event.target.value, page: 1}, () => this.getRoutesList())
    }

    // toggle checkbox and reverse results
    handleCheck(e){
        this.setState({
            descend: !this.state.descend
        }, () => this.getRoutesList())
    }

    handlePag(event, page) {
        this.setState({page: page}, () => this.getRoutesList())
    }

    toggleCompareItem(item) {
        // if the item is there, remove it
        if (this.state.compare.includes(item)) {
            this.setState({ compare: this.state.compare.filter((comp) => comp !== item) })
        }
        // if the item is not there, add it
        else {
            this.setState({ compare: [...this.state.compare, item]})
        }
    }

    togglePopup(event) {
        if(this.state.compare.length > 0) {
            this.setState({ popup: !this.state.popup })
        }
    }

    render() {
        const cityOptions = [['austin', 'Austin'], ['houston', "Houston"], ['san antonio', 'San Antonio'], ['dallas', 'Dallas']]
        const sortOptions = [['id','ID'], ['city', 'City'], ['origin', 'Origin Name'], ['destination', 'Destination Name']]
        const tableHeaders = ['ID', 'City', 'Origin', 'Destination', 'Departure Time (CST)', 'Arrival Time (CST)', 'Duration', 'Modes', 'Compare']

        return (
            <div>
                <h1>Transit</h1>
                {this.state.popup ? <Popup closePopup={this.togglePopup.bind(this)} type='transit' items={this.state.compare}/> :
                <Container>
                    <Row className='form'>
                        {/* sorting options */}
                        <FormControl className='formItem'>
                            <InputLabel>
                                Sort By
                            </InputLabel>
                            <Select
                            defaultValue={'id'}
                            onChange={this.sortBy.bind(this)}
                            >
                            {sortOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                            </Select>
                            <FormControlLabel control={<Checkbox name="checked" color="primary" onClick={this.handleCheck.bind(this)}/>} label="Descending" />
                        </FormControl>

                        {/* filter by city */}
                        <FormControl className='formItem'>
                            <InputLabel>
                                City
                            </InputLabel>
                            <Select
                            onChange={this.filterByCity.bind(this)}
                            defaultValue={""}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {cityOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <input className='input' autoComplete="off" placeholder="Search"type="text" value={this.state.search} onChange={this.handleSearch.bind(this)}/>
                        <Button variant="contained" className='compareButton' onClick={this.togglePopup.bind(this)}>Compare</Button>
                        <img className='algolia' src={algoliaImg} alt='algolia icon'/>
                    </Row>
                    <br></br>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                {tableHeaders.map((item, idx) => <th key={idx}>{item}</th>)}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.routes.map((r) => <TransitRow key={r.id} route={r} id={r.id} search={this.state.search} handleCompare={this.toggleCompareItem.bind(this)}/>)}
                        </tbody>
                    </Table>
                    <br/>
                    <ModelFooter pages={this.state.pages} total={this.state.total} handlePag={this.handlePag.bind(this)} />
                </Container>}
            </div>)
    }
}

export default Transit
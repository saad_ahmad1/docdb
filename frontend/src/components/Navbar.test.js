
import React from 'react'
import Navbar from './Navbar'
import { BrowserRouter } from 'react-router-dom'

import { mount, render, shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Navbar component test suite', () => {
    configure({ adapter: new Adapter() })
    it('tests if the navbar conforms to the correct classes', () => {
        let navbar = render(<BrowserRouter><Navbar /></BrowserRouter>)
        expect(navbar.hasClass('navbar')).toBe(true)
        expect(navbar.hasClass('navbar-expand-lg')).toBe(true)
        expect(navbar.hasClass('navbar-dark')).toBe(true)
        expect(navbar.hasClass('bg-custom')).toBe(true)
    })

    it('tests if the navbar contains four navbar items', () => {
        let navbar = render(<BrowserRouter><Navbar /></BrowserRouter>)
        expect(navbar.find('.nav-item').length).toBe(6)
    })
})
module.exports = api => {
    const presets = [
        '@babel/preset-env',
    ]
    const plugins = ['@babel/plugin-transform-react-jsx']
    api.cache(false)
    return {
        presets,
        plugins
    }
}
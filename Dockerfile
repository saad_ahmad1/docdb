FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/groverhood/docdb.git

WORKDIR /docdb

RUN git pull --force

RUN cd frontend && yarn install && yarn build

RUN pip3 --no-cache-dir install -r backend/requirements.txt

EXPOSE 80

CMD git pull --force && python3 backend/main.py